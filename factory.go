package event

// Factory is a factory for events
type Factory struct {
	connState     connState
	eventFeedback chan Event // Channel for Feedback
	eventControl  chan Event // Channel for Control
}

type FactoryConfig struct {
	ControlQueueSize  int
	FeedbackQueueSize int
}

// New returns a new EventFactory which handles events for a device
func New() *Factory {
	c := FactoryConfig{
		ControlQueueSize:  25,
		FeedbackQueueSize: 25,
	}
	return NewWithConfig(c)
}

// NewWithConfig returns a new EventFactory which handles events for a device
func NewWithConfig(c FactoryConfig) *Factory {
	return &Factory{
		eventFeedback: make(chan Event, c.FeedbackQueueSize),
		eventControl:  make(chan Event, c.ControlQueueSize),
	}
}

// Feedback returns the feedback channel
func (f *Factory) Feedback() <-chan Event {
	return f.eventFeedback
}

// Control returns the control channel
func (f *Factory) Control() chan<- Event {
	return f.eventControl
}

// SetConnection sets the current connection state
func (f *Factory) SetConnection(state connState) {
	// set state
	if state == f.connState {
		return
	}
	f.connState = state
	// make event
	e := connectionEvent(state)
	// send event
	f.eventFeedback <- e
}

// GetConnection returns the current connection state
func (f *Factory) GetConnection() connState {
	return f.connState
}

// SendMetaDataFB sends one item of metadata to the feedback channel
func (f *Factory) SendMetaDataFB(key string, value interface{}) {
	f.SendMetaDataMapFB(map[string]interface{}{key: value})
}

// SendMetaDataMapFB sends metadata to the feedback channel
func (f *Factory) SendMetaDataMapFB(data map[string]interface{}) {
	e := Event{
		Type:   TypeMetadata,
		Action: ActionUpdate,
		Data:   data,
	}
	f.SendFeedback(e)
}

// SendStateFB sends one item of state to the feedback channel
func (f *Factory) SendStateFB(key string, value interface{}) {
	f.SendStateMapFB(map[string]interface{}{key: value})
}

// SendStateFB sends state to the feedback channel
func (f *Factory) SendStateMapFB(data map[string]interface{}) {
	e := Event{
		Type:   TypeState,
		Action: ActionUpdate,
		Data:   data,
	}
	f.SendFeedback(e)
}

func (f *Factory) SendFeedback(e Event) {
	f.eventFeedback <- e
}
