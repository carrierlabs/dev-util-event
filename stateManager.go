package event

import "time"

type connState string

const (
	// ConnStateOnline indicates the connection is online
	ConnStateOnline connState = "Online"
	// ConnStateOffline indicates the connection is offline
	ConnStateOffline connState = "Offline"
	// ConnStateTimedout indicates the connection has timed out
	ConnStateTimedout connState = "Timedout"
)

// StateManager handles current state events for a device, with functions to control without exposing timers
type StateManager struct {
	connTimer *time.Timer
	config    StateManagerConfig
}

// StateManagerConfig configures a enw StateManager
type StateManagerConfig struct {
	EventChan chan Event
	Timeout   time.Duration
	CallBack  func()
}

// Online sets connetion online and sends event, or resets timer if already live
func (sm *StateManager) Online() {
	// Kill timer if running
	if sm.connTimer == nil {

		// Trigger Online Event
		sm.config.EventChan <- connectionEvent(ConnStateOnline)

		// Create timer and spin up go routine to watch it
		sm.connTimer = time.NewTimer(sm.config.Timeout)

		// Start Go routine to watch for offline
		go func() {
			// Wait for timeout
			<-sm.connTimer.C
			// Dispose of the timer
			sm.connTimer = nil
			// Send Event
			sm.config.EventChan <- connectionEvent(ConnStateTimedout)
			// Run Callback if present
			if sm.config.CallBack != nil {
				sm.config.CallBack()
			}
		}()
	} else {
		// Stop and Restart Timer
		sm.connTimer.Stop()
		sm.connTimer.Reset(sm.config.Timeout)
	}
}

// NewStateManager returns a StateManager which will raise events on the channel provided
func NewStateManager(config StateManagerConfig) *StateManager {
	return &StateManager{config: config}
}

func connectionEvent(state connState) Event {
	return Event{
		Action: ActionUpdate,
		Type:   TypeSystem,
		Data: map[string]interface{}{
			"ConnState": connState(state),
		},
	}
}
