package event

import (
	"strings"
)

// Device interface provides methods to control and monitor devices using this package
type Device interface {
	EventControl() chan<- Event
	EventFeedback() <-chan Event
}

// Action indicates what this event is doing
type Action int

const (
	// ActionUpdate indicates an Update
	ActionUpdate Action = iota
	// ActionCreate indicates an Update
	ActionCreate
	// ActionDelete indicates an Update
	ActionDelete
)

// DataAsBytes Returns Value as type Int, or nil
func (e *Event) DataAsBytes(key string) ([]byte, bool) {
	b, ok := e.Data[key].([]byte)
	return b, ok
}

// DataAsString Returns Value as type Int, or nil
func (e *Event) DataAsString(key string) (string, bool) {
	s, ok := e.Data[key].(string)
	return s, ok
}

// DataAsInt Returns Value as type Int, or nil
func (e *Event) DataAsInt(key string) (int, bool) {
	i, ok := e.Data[key].(int)
	return i, ok
}

// DataAsFloat Returns Value as type Int, or nil
func (e *Event) DataAsFloat(key string) (float64, bool) {
	f, ok := e.Data[key].(float64)
	return f, ok
}

// DataAsBool Returns Value as type Int, or nil
func (e *Event) DataAsBool(key string) (bool, bool) {
	b, ok := e.Data[key].(bool)
	return b, ok
}

// DataAsDevice Returns Value as type Int, or nil
func (e *Event) DataAsDevice(key string) (Device, bool) {
	d, ok := e.Data[key].(Device)
	return d, ok
}

// Event contains relevant data when raised on the Events channel
type Event struct {
	Type     Type                   // Event Type
	Action   Action                 // Event Action
	Target   string                 // Target object
	TargetID string                 // Target ID
	Hint     string                 // Custom Additional hint for target or subject, to aid processing
	Data     map[string]interface{} // Event Value Data
}

// Type identifies what raised an event
type Type int

const (
	// TypeUndefined indicates empty event
	TypeUndefined Type = iota
	// TypeSystem indicates a change to a system state
	TypeSystem
	// TypeMetadata indicates new or updated metadata
	TypeMetadata
	// TypeState indicates a state change (Control and Feedback)
	TypeState
	// TypeChild indicates a state change (Control and Feedback)
	TypeChild
)

// TypeString Returns Type of event as String
func (e *Event) TypeString() string {

	var sb strings.Builder

	// Add Event Type
	switch e.Type {
	case TypeUndefined:
		sb.WriteString("Undefined")
	case TypeSystem:
		sb.WriteString("System")
	case TypeMetadata:
		sb.WriteString("MetaData")
	case TypeState:
		sb.WriteString("State")
	case TypeChild:
		sb.WriteString("Child")
	}

	// Add Event Action
	switch e.Action {
	case ActionCreate:
		sb.WriteString("Create")
	case ActionUpdate:
		sb.WriteString("Update")
	case ActionDelete:
		sb.WriteString("Delete")
	}

	// Return default
	return sb.String()
}
